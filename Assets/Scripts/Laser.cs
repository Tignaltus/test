using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Laser : MonoBehaviour
{
    [SerializeField] private List<AudioClip> audioClips = new List<AudioClip>();
    private AudioSource _audioSource;
    private float lifeTime = 5f;
    public int team;
    private float dmg = 10f;
    public GameObject explosionPrefab;
    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        Destroy(gameObject, lifeTime);
        _audioSource.clip = audioClips[Random.Range(0, audioClips.Count)];
        _audioSource.Play();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ship"))
        {
            if (other.gameObject.GetComponent<ShootLaser>().team != team)
            {
                try
                {
                    other.gameObject.GetComponent<Fighter>().TakeDamage(dmg);
                }
                catch 
                {
                    other.gameObject.GetComponent<Cruiser>().TakeDamage(dmg);
                }
                Instantiate(explosionPrefab, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }
}
