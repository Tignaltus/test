using System;
using UnityEngine;
using UnityEngine.Audio;

public class SetVolume : MonoBehaviour
{
     [SerializeField] private AudioMixer bgmMixer, sfxMixer;

     private void Start()
     {
          SetBGMLevel(0.5f);
          SetSFXLevel(0.5f);
     }

     public void SetSFXLevel(float sliderValue)
     {
          sfxMixer.SetFloat("SFXVolume", Mathf.Log10(sliderValue) * 20);
     }

     public void SetBGMLevel(float sliderValue)
     {
          bgmMixer.SetFloat("BGMVolume", Mathf.Log10(sliderValue) * 20);
     }
}