using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    private int count = 0;
    private Ship selectedShip;
    
    
    [SerializeField] private GameObject container, shipButton, startBattleButton;
    private List<GameObject> buttonList = new List<GameObject>();

    public void PopulateGridList()
    {
        foreach (var ship in GameManager.instance.GetShipManager().listOfShipsTI)
        {
            var temp = count;
            buttonList.Add(Instantiate(shipButton, container.transform));
            buttonList[count].GetComponent<Button>().onClick.AddListener(delegate { SetActiveShip(temp); });
            switch (ship)
            {
                case Fighter fighter:
                    buttonList[count].GetComponentInChildren<TextMeshProUGUI>().text = $"Fighter {count}";
                    break;
                case Cruiser cruiser:
                    buttonList[count].GetComponentInChildren<TextMeshProUGUI>().text = $"Cruiser {count}";
                    break;
            }
            count++;
        }
    }

    public void SetActiveShip(int myIndex)
    {
        selectedShip = GameManager.instance.GetShipManager().listOfShipsTI[myIndex];
        EventHandler.Instance.ClearGridSelection();
        if (selectedShip.GetStartPoint())
        {
            selectedShip.GetStartPoint().gameObject.GetComponent<SphereSelection>().SetSphereActive();
        }

        if (selectedShip.GetEndPoint())
        {
            selectedShip.GetEndPoint().gameObject.GetComponent<SphereSelection>().SetSphereActive();
        }
    }

    private void OnGridSphereClick(GameObject point)
    {
        if (selectedShip == null) return;
        if (selectedShip.GetStartPoint() == point.transform)
        {
            selectedShip.SetStartPoint(null);
        }
        else if(selectedShip.GetEndPoint() == point.transform)
        {
            selectedShip.SetEndPoint(null);
        }
        
        if (!selectedShip.GetStartPoint())
        {
            selectedShip.SetStartPoint(point.transform);
        }
        else if (!selectedShip.GetEndPoint())
        {
            selectedShip.SetEndPoint(point.transform);
        }

        startBattleButton.GetComponent<Button>().interactable = true;
        foreach (var ship in GameManager.instance.GetShipManager().listOfShipsTI)
        {
            if (!ship.GetStartPoint() || !ship.GetEndPoint())
            {
                startBattleButton.GetComponent<Button>().interactable = false;
            }
        }
    }

    private void OnEnable()
    {
        EventHandler.Instance.OnGridSphereClick += OnGridSphereClick;
    }

    private void OnDisable()
    {
        EventHandler.Instance.OnGridSphereClick -= OnGridSphereClick;
    }
}
