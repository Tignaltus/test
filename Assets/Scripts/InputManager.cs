using UnityEngine;

public class InputManager : MonoBehaviour
{
    [Header("Camera Controls")]
    [SerializeField] private GameObject cameraRig;
    [SerializeField] private float movementTime = 10, zoomAmount = 2, maxZoom = 20, minZoom = 5;
    private Camera _camera;
    private Vector3 _newPosition, _dragStartPosition, _dragCurrentPosition, _zoom, _newZoom;

    private void Awake()
    {
        _camera = Camera.main;
        _camera.transform.position = cameraRig.transform.position - _camera.transform.forward * 15;
        _newPosition = cameraRig.transform.position;
        _newZoom = _zoom = _camera.transform.localPosition;
    }

    private void Update()
    {
        HandleMouseInput();
    }

    private void LateUpdate()
    {
        //===============Smooth camera movement===============//
        //Zooming
        /*
        _camera.transform.localPosition =
            Vector3.Lerp(_camera.transform.localPosition, _zoom, Time.deltaTime * movementTime);
        //Panning
        cameraRig.transform.position = 
        Vector3.Lerp(cameraRig.transform.position, _newPosition, Time.deltaTime * movementTime);
        */
    }

    private void HandleMouseInput()
    {
        #region Left Mouse Button

        if (Input.GetMouseButtonDown(0))
        {
            if (!Physics.Raycast(_camera.ScreenPointToRay(Input.mousePosition), out var hit)) return;
            if (hit.transform == null) return;
            if (hit.transform.gameObject.layer != 7) return;
            //If click hit a transform and the transform is on layer 7 (Interactable)
            if (hit.transform.gameObject.CompareTag("Grid"))
            {
                hit.transform.gameObject.GetComponent<SphereSelection>().SphereClick();
            }
        }

        #endregion
        
        #region ScrollWheel

        if (Input.mouseScrollDelta.y != 0) //Zoom on scrolling
        {
            _newZoom += Input.mouseScrollDelta.y * _camera.transform.forward * zoomAmount;
            CalculateNewZoomDistance();
        }

        #endregion

        #region Right Mouse Button

        if (Input.GetMouseButtonDown(1)) //Pan camera with right mouse button
        {
            var plane = new Plane(Vector3.up, Vector3.zero);
            var ray = _camera.ScreenPointToRay(Input.mousePosition);
            if (plane.Raycast(ray, out var entry))
            {
                _dragStartPosition = ray.GetPoint(entry);
            }
        }
        if (Input.GetMouseButton(1))
        {
            var plane = new Plane(Vector3.up, Vector3.zero);
            var ray = _camera.ScreenPointToRay(Input.mousePosition);
            if (!plane.Raycast(ray, out var entry)) return;
            _dragCurrentPosition = ray.GetPoint(entry);

            _newPosition = cameraRig.transform.position + _dragStartPosition - _dragCurrentPosition;
        }

        #endregion
    }

    private void CalculateNewZoomDistance()
    {
        var a = _newZoom.y;
        var b = _newZoom.z;
        var c = Mathf.Sqrt(a*a + b*b);
        if (c > minZoom && c < maxZoom)
        {
            _zoom = _newZoom;
        }
        else
        {
            _newZoom = _zoom;
        }
    }
}