using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class ShipManager : MonoBehaviour
{
    public GridSystem gridSystem;
    public List<Ship> listOfShipsTI;
    public List<Ship> listOfShipsTII = new List<Ship>();
    public GameObject explosionPrefab;

    [Space] 
    
    [SerializeField] private Ship fighterPrefab;
    [SerializeField] private Ship cruiserPrefab;
    [SerializeField] private TextMeshProUGUI winnerText;
    [SerializeField] private GameObject winnerWindow;
    public bool _runSimulation = false;

    // === PRIVATE METHODS ===
    
    private void Start()
    {
        foreach (var ship in listOfShipsTI)
        {
            ship.Initialize(1);
        }

        foreach (var ship in listOfShipsTII)
        {
            ship.Initialize(2);
        }
    }

    private void FixedUpdate()
    {
        if (!_runSimulation) return;
        UpdateShips();
        TeamsAlive();
    }

    private void UpdateShips()
    {
        foreach (var ship in listOfShipsTI)
        {
            ship.UpdateSystems();
        }

        foreach (var ship in listOfShipsTII)
        {
            ship.UpdateSystems();
        }
    }

    private void TeamsAlive()
    {
        var teamOneAlive = false;
        foreach (var ship in listOfShipsTI)
        {
            if (ship.gameObject.activeSelf)
            {
                teamOneAlive = true;
            }
        }

        var teamTwoAlive = false;
        foreach (var ship in listOfShipsTII)
        {
            if (ship.gameObject.activeSelf)
            {
                teamTwoAlive = true;
            }
        }

        if (!teamOneAlive)
        {
            winnerText.text = $"Opponent won";
            winnerWindow.gameObject.SetActive(true);
            winnerText.gameObject.SetActive(true);
            StartCoroutine(EndTimer());
        }
        else if (!teamTwoAlive)
        {
            winnerText.text = $"You won";
            winnerText.gameObject.SetActive(true);
            winnerWindow.gameObject.SetActive(true);
            StartCoroutine(EndTimer());
        }
    }

    private IEnumerator EndTimer()
    {
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    // === PUBLIC METHODS ===

    public void CreateFighter(int position)
    {
        var posVector = new Vector3(position*2, 0, -2);
        listOfShipsTI.Add(Instantiate(fighterPrefab, posVector, quaternion.identity));
    }
    
    public void CreateCruiser(int position)
    {
        var posVector = new Vector3(position * 2, 0, -3);
        listOfShipsTI.Add(Instantiate(cruiserPrefab, posVector, Quaternion.identity));
    }

    public void BattleStartSetup()
    {
        //setup player ships
        foreach (var ship in listOfShipsTI)
        {
            ship.gameObject.transform.position = ship.GetStartPoint().position;
            ship.gameObject.transform.LookAt(ship.GetEndPoint());
            ship.Initialize(1);
            ship.gameObject.GetComponent<ShootLaser>().team = 1;
        }
        
        //Setup enemy ships
        var randomAmount = Random.Range(listOfShipsTI.Count - 2, listOfShipsTI.Count + 2);
        randomAmount = Mathf.Clamp(randomAmount, 1, 100);
        for (var i = 0; i < randomAmount; i++)
        {
            var randomTemp = Random.Range(1, 3);
            listOfShipsTII.Add(randomTemp == 1 ? Instantiate(fighterPrefab) : Instantiate(cruiserPrefab));
            listOfShipsTII[i].SetStartPoint(gridSystem._gridSpheres[Random.Range(0, gridSystem._gridSpheres.Count)].transform);
            listOfShipsTII[i].SetEndPoint(gridSystem._gridSpheres[Random.Range(0, gridSystem._gridSpheres.Count)].transform);
            listOfShipsTII[i].transform.position = listOfShipsTII[i].GetStartPoint().position;
            listOfShipsTII[i].transform.LookAt(listOfShipsTII[i].GetEndPoint());
            listOfShipsTII[i].Initialize(2);
            listOfShipsTII[i].gameObject.GetComponent<ShootLaser>().team = 2;
        }
        
        EventHandler.Instance.ClearGridSpheres();
        _runSimulation = true;
    }
}


