using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Cruisers - Medium mobile ships with moderate firepower.

public class Cruiser : Ship
{
    //public GameObject explosionPrefab;
    public override void Initialize(int assignTeam)
    {
        base.Initialize(assignTeam);
    }

    public override void UpdateSystems()
    {
        base.UpdateSystems();
        Movement();
    }

    private void Movement()
    {
        transform.position += transform.forward * shipMobility * Time.deltaTime;
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Ship"))
        {
            Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            Debug.Log("Crash!");
            gameObject.SetActive(false);
        }
    }
}
