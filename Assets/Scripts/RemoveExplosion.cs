using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class RemoveExplosion : MonoBehaviour
{
    [SerializeField] private float lifeTime = 1.5f;
    [SerializeField] private List<AudioClip> audioClips = new List<AudioClip>();
    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        Destroy(gameObject, lifeTime);
    }

    void Start()
    {
        _audioSource.clip = audioClips[Random.Range(0, audioClips.Count)];
        _audioSource.Play();
    }

}
