using System;
using System.Collections.Generic;
using UnityEngine;

public class GridSystem : MonoBehaviour
{
    [SerializeField] private GameObject gridSelectPrefab;

    [Header("Grid size")] 
    [SerializeField] [Range(1, 50)] private int columnAmount = 20;
    [SerializeField] [Range(1, 50)] private int rowAmount = 20;
    [SerializeField] [Range(0.5f, 5f)] private float xSpacing = 1f, ySpacing = 1f;

    public readonly List<GameObject> _gridSpheres = new List<GameObject>();

    private void OnEnable() => EventHandler.Instance.OnClearGridSpheres += ClearGridSpheres;
    private void OnDisable() => EventHandler.Instance.OnClearGridSpheres -= ClearGridSpheres;

    private void Start()
    {
        for (var i = 0; i < columnAmount*rowAmount; i++)
        {
            _gridSpheres.Add(Instantiate(gridSelectPrefab, new Vector3(xSpacing * (i % columnAmount),
                0f, ySpacing * (i / columnAmount)), Quaternion.identity));
        }
    }

    private void ClearGridSpheres() //Call this by invoking the event
    {
        foreach (var go in _gridSpheres)
        {
            Destroy(go);
        }
        _gridSpheres.Clear();
    }
}