using UnityEngine;

public class SphereSelection : MonoBehaviour
{
    [SerializeField] private Material notSelected, selected;
    private bool _isSelected;

    private void OnEnable() => EventHandler.Instance.OnClearGridSelection += ClearGridSelection;
    private void OnDisable() => EventHandler.Instance.OnClearGridSelection -= ClearGridSelection;

    public void SphereClick()
    {
        _isSelected = !_isSelected;
        GetComponent<MeshRenderer>().material = _isSelected? selected : notSelected;
        EventHandler.Instance.GridSphereClick(gameObject);
    }

    private void ClearGridSelection() //Call this by invoking the event
    {
        if (!_isSelected) return;
        _isSelected = false;
        GetComponent<MeshRenderer>().material = notSelected;
    }

    public void SetSphereActive()
    {
        _isSelected = true;
        GetComponent<MeshRenderer>().material = selected;
    }
}
