using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BGMusicSelector : MonoBehaviour
{
     [SerializeField] private List<AudioClip> tracks = new List<AudioClip>();
     private int _index;
     private AudioSource _audioSource;

     private void Awake()
     {
          _audioSource = GetComponent<AudioSource>();
     }

     private void Start()
     {
          _index = Random.Range(0, tracks.Count);
          PlayNextTrack();
     }

     private void Update()
     {
          if (_audioSource.isPlaying) return;
          PlayNextTrack();
     }

     private void PlayNextTrack()
     {
          _audioSource.clip = tracks[_index];
          _audioSource.Play();
          _index++;
          if (_index == tracks.Count)
               _index = 0;
     }
}