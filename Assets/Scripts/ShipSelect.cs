using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShipSelect : MonoBehaviour
{
    [SerializeField] private GameObject container;
    
    [Space]
    
    [SerializeField] private GameObject fighterIcon;
    [SerializeField] private GameObject cruiserIcon;

    [Space] 
    
    [SerializeField] private List<GameObject> fleetRoster;
    private List<int> fleetIndex;

    public void AddFighter()
    {
        GameObject fighterButton = Instantiate(fighterIcon, container.transform);
        fighterButton.GetComponent<Button>().onClick.AddListener(delegate { DeleteSelf(fighterButton); });
        
        fleetRoster.Add(fighterButton);
    }

    public void AddCruiser()
    {
        GameObject cruiserButton = Instantiate(cruiserIcon, container.transform);
        cruiserButton.GetComponent<Button>().onClick.AddListener(delegate { DeleteSelf(cruiserButton); });
        
        fleetRoster.Add(cruiserButton);
    }

    public void DeleteSelf(GameObject obj)
    {
        fleetRoster.Remove(obj);
        Destroy(obj);
    }

    public void BeginBattle()
    {
        var temp = 0;
        foreach (var button in fleetRoster)
        {
            
            if (button.GetComponentInChildren<TextMeshProUGUI>().text == "Fighter")
            {
                GameManager.instance.GetShipManager().CreateFighter(temp);
            }
            else if (button.GetComponentInChildren<TextMeshProUGUI>().text == "Cruiser")
            {
                GameManager.instance.GetShipManager().CreateCruiser(temp);
            }

            temp++;
        }
    }
}
