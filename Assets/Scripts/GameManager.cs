using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [SerializeField] private ShipManager _shipManager;
    
    // === PRIVATE METHODS === //
    
    private void Awake()
    {
        instance = this;
    }

    // === PUBLIC METHODS === //
    
    public ShipManager GetShipManager()
    {
        return _shipManager;
    }
}
