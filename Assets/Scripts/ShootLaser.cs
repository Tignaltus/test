using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ShootLaser : MonoBehaviour
{
    private bool start = true;
    public int team;
    // drag-ins
    [SerializeField] private GameObject laser;
    [SerializeField] private Transform projectilePos;
    
    //bullet force
    public float shootForce, upwardForce;
    
    //Gun stats
    public float timeBetweenShooting;
    

    private void Start()
    {
        
    }

    private void Update()
    {
        if (GameManager.instance.GetShipManager()._runSimulation && start)
        {
            StartCoroutine(Shoot());
            start = false;
        }
    }

    public IEnumerator Shoot()
    { 
        while (true)
        {
            // start shooting in random intervals
            timeBetweenShooting = Random.Range(1f, 4f);
            yield return new WaitForSeconds(timeBetweenShooting);
            //currentLaser.transform.forward = transform.up;
            ShipAim(GetClosestTarget());
            GameObject currentLaser = Instantiate(laser, projectilePos.position, projectilePos.rotation);
            currentLaser.GetComponent<Laser>().team = team;
            SetSpread(currentLaser);
            currentLaser.GetComponent<Rigidbody>().AddForce(currentLaser.transform.forward * shootForce, ForceMode.Impulse);
            
        }
    }

    public void SetSpread(GameObject obj)
    {
        var ranX = Random.Range(-10, 10);
        var ranY = Random.Range(-10, 10);
        var ranZ = Random.Range(-10, 10);
        obj.transform.Rotate(ranX,ranY,ranZ);
    }

    public Transform GetClosestTarget()
    {
        Transform target = null;
        float closestDist = 10000000000f;
        if (team == 1)
        {
            for (int i = 0; i < GameManager.instance.GetShipManager().listOfShipsTII.Count; i++)
            {
                if (!GameManager.instance.GetShipManager().listOfShipsTII[i].gameObject.activeSelf) { continue; }
                float dist = Vector3.Distance(GameManager.instance.GetShipManager().listOfShipsTII[i].transform.position,
                    transform.position);
                if (dist < closestDist)
                {
                    closestDist = dist;
                    target = GameManager.instance.GetShipManager().listOfShipsTII[i].transform;
                }
            }
        }
        else if (team == 2)
        {
            for (int i = 0; i < GameManager.instance.GetShipManager().listOfShipsTI.Count; i++)
            {
                if (!GameManager.instance.GetShipManager().listOfShipsTI[i].gameObject.activeSelf) { continue; }
                float dist = Vector3.Distance(GameManager.instance.GetShipManager().listOfShipsTI[i].transform.position,
                    transform.position);
                if (dist < closestDist)
                {
                    closestDist = dist;
                    target = GameManager.instance.GetShipManager().listOfShipsTI[i].transform;
                }
            }  
        }
        Debug.Log(target);
        return target;
    }

    public void ShipAim(Transform target)
    {
        if (target == null)return;
        
        Vector3 targetDirection = (target.position - projectilePos.transform.position).normalized;
        



        projectilePos.transform.rotation = Quaternion.LookRotation(targetDirection);
    }
}
