using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ship : MonoBehaviour
{
    [SerializeField] protected int team;
    [SerializeField] protected float shipHealth;
    [SerializeField][Range(0,1)] protected float shipDefence;
    [SerializeField] protected float shieldStrength;
    
    [Space]
    
    [SerializeField] protected float shipMobility;
    [SerializeField] protected float resources;
    [SerializeField] protected float coc;
    [SerializeField] protected GameObject explosionPrefab;

    protected Transform startPoint;
    protected Transform endPoint;
    protected Vector3 endPointVector;

    /// <summary>
    /// This Method is used to initialize any variables before the game starts.
    /// </summary>
    public virtual void Initialize(int assignTeam)
    {
        team = assignTeam;
    }

    
    /// <summary>
    /// This Method is supposed to be called from outside and Update all Methods that needs to be updated.
    /// </summary>
    public virtual void UpdateSystems()
    {
        if (Vector3.Distance(gameObject.transform.position, endPointVector) < 0.1f)
        {
            shipMobility = 0;
        }
    }

    public virtual Transform GetStartPoint()
    {
        return startPoint;
    }    
    
    public virtual void SetStartPoint(Transform transPoint)
    {
        startPoint = transPoint;
    }

    public virtual Transform GetEndPoint()
    {
        return endPoint;
    }

    public virtual void SetEndPoint(Transform transPoint)
    {
        endPoint = transPoint;
        endPointVector = transPoint.position;
    }

    public virtual void TakeDamage(float damage)
    {
        if (shieldStrength > 0)
        {
            shieldStrength -= damage;
            return;
        }
        damage -= damage * shipDefence;
        shipHealth -= damage;
        if (shipHealth <= 0)
        {
            Instantiate(explosionPrefab, transform.position, Quaternion.identity);
            gameObject.SetActive(false);
        }
    }
}
