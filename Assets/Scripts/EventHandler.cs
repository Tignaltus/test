using System;
using UnityEngine;

[DefaultExecutionOrder(-1)]
public class EventHandler : MonoBehaviour
{
    public static EventHandler Instance;
    private void Awake() => Instance = this;
    
    //==========EVENTS==========//
    public event Action<GameObject> OnGridSphereClick;
    public void GridSphereClick(GameObject go) => OnGridSphereClick?.Invoke(go);

    public event Action OnClearGridSelection;
    public void ClearGridSelection() => OnClearGridSelection?.Invoke();

    public event Action OnClearGridSpheres;
    public void ClearGridSpheres() => OnClearGridSpheres?.Invoke();
}