using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PathFollow : MonoBehaviour
{
    
    private Transform targetPoint;
    private List<GameObject> ships = new List<GameObject>();
    private int index;
    private bool move = false;
    private bool start = false;
    private Vector3 midpoint;
    private Camera cam;
    private float zoom;
    private float offset = 100f;
    
    //public variables
    public float speed = 3f;
    public Transform pathParent;
    public float zoomSpeed = 1f;
    private void OnDrawGizmos()
    {
        Vector3 from;
        Vector3 to;
        for (int i = 0; i < pathParent.childCount; i++)
        {
            //See path of movement in scene-view
            from = pathParent.GetChild(i).position;
            to = pathParent.GetChild((i + 1) % pathParent.childCount).position;
            Gizmos.color = new Color(1, 0, 0);
            Gizmos.DrawLine(from, to);
        }
        Gizmos.DrawLine(transform.position, midpoint);
    }
    private void Start()
    {
        index = 0;
        targetPoint = pathParent.GetChild(index);
        cam = Camera.main;
        zoom = cam.fieldOfView;

    }
    private void LateUpdate()
    {
        StartMovingCamera();
        MoveCamera();
        RotateCamera();
        //ZoomCamera();
    }
    private void StartMovingCamera()
    {
        if (GameManager.instance.GetShipManager()._runSimulation)
        {
            move = true;
            return;
        }

        move = false;
    }

    private void MoveCamera()
    {
        if (move)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPoint.position, speed * Time.deltaTime);
            if (Vector3.Distance(transform.position, targetPoint.position) < 0.1f)
            {
                index++;
                index %= pathParent.childCount;
                targetPoint = pathParent.GetChild(index);
            }
        }
    }
    private void RotateCamera()
    {
        if (GameManager.instance.GetShipManager()._runSimulation && !start)
        {
            if (!start)
            {
                foreach (GameObject ship in GameObject.FindGameObjectsWithTag("Ship"))
                {
                    ships.Add(ship);
                }
            }
        }
        float posX = 0f;
        float posY = 0f;
        float posZ = 0f;
        
        foreach (GameObject ship in ships)
        {
            posX += ship.transform.position.x;
            posY += ship.transform.position.y;
            posZ += ship.transform.position.z;
        }

        float centerX = posX / ships.Count;
        float centerY = posY / ships.Count;
        float centerZ = posZ / ships.Count;
        midpoint = new Vector3(centerX, centerY, centerZ);
        transform.LookAt(midpoint);
    }

    private void ZoomCamera()
    {
        if (GameManager.instance.GetShipManager()._runSimulation)
        {
            List<Vector3> shipPos = new List<Vector3>();
            List<bool> onScreen = new List<bool>();
            foreach (GameObject ship in ships)
            {
                shipPos.Add(ship.transform.position);
            }

            if (ships.All(obj => obj == IsInScreen(obj)))
            {
                zoom -= 5;
                cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, zoom, Time.deltaTime * zoomSpeed);
            }
            else if (ships.All(obj => obj != IsInScreen(obj)))
            {
                zoom += 5;
                cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, zoom, Time.deltaTime * zoomSpeed);
            }
            shipPos.Clear();
        }
    }

    private bool IsInScreen(GameObject obj)
    {
        Vector3 screenPos = cam.WorldToScreenPoint(obj.transform.position);
        bool onScreen = screenPos.x > offset && screenPos.x < Screen.width - offset && screenPos.y > offset && screenPos.y < Screen.height - offset;
 
        if (onScreen)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
